package org.testjava.apijeudecartes.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.testjava.apijeudecartes.mapper.CardViewMapper;
import org.testjava.apijeudecartes.models.CardImpl;
import org.testjava.apijeudecartes.models.CardView;
import org.testjava.apijeudecartes.models.types.CardValues;
import org.testjava.apijeudecartes.models.types.Cards;
import org.testjava.apijeudecartes.models.types.Colors;
import org.testjava.apijeudecartes.models.types.HandTypes;

@Service
public class CardServices {

  public Map<String, List<CardView>> createCardsListAndGetHand(int numberOfCards) {

    final var cardsList = new ArrayList<Cards>();
    for (Colors color : Colors.values()) {
      for (CardValues cardValue : CardValues.values()) {
        cardsList.add(new CardImpl(color, cardValue));
      }
    }
    Collections.shuffle(cardsList);
    final var hand = cardsList.subList(0, numberOfCards);
    List<Cards> sortHand = new ArrayList<>(hand);
    Collections.sort(sortHand);

    return Map.of(HandTypes.TRIE.name(), sortHand
        .stream()
        .map(CardViewMapper::mapToCardView)
        .toList(), HandTypes.NON_TRIE.name(), hand
        .stream()
        .map(CardViewMapper::mapToCardView)
        .toList());
  }
}
