package org.testjava.apijeudecartes.mapper;

import org.testjava.apijeudecartes.models.CardView;
import org.testjava.apijeudecartes.models.types.Cards;

public class CardViewMapper {

  private CardViewMapper() {

    throw new IllegalStateException("Utility class");
  }

  public static CardView mapToCardView(Cards card) {

    if (card == null) {
      return null;
    }

    return new CardView(card
        .getColor()
        .getColorValue(), card
        .getCardValue()
        .getValue());
  }
}
