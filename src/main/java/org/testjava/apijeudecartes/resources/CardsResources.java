package org.testjava.apijeudecartes.resources;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.testjava.apijeudecartes.models.CardView;
import org.testjava.apijeudecartes.models.types.Cards;
import org.testjava.apijeudecartes.services.CardServices;

@RestController
@RequestMapping("/api/card/v1")
@RequiredArgsConstructor
@CrossOrigin("*")
public class CardsResources {

  private final CardServices cardServices;

  @GetMapping(value = "/hand/{numbersOfCard}")
  @Operation(summary = "Get hand of {numbersOfCard} cards")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "success",
              content = {
                  @Content(array = @ArraySchema(schema = @Schema(implementation = Cards.class)))
              }),
          @ApiResponse(responseCode = "400", description = "bad request", content = @Content),
          @ApiResponse(responseCode = "403", description = "forbidden", content = @Content)
      })
  public Map<String, List<CardView>> getHands(@PathVariable("numbersOfCard") int numbersOfCard) {

    return cardServices.createCardsListAndGetHand(numbersOfCard);
  }
}
