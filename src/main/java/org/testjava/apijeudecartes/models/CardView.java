package org.testjava.apijeudecartes.models;

public class CardView {

  private final String color;
  private final String cardValue;

  public CardView(String color, String cardValue) {

    this.color = color;
    this.cardValue = cardValue;
  }

  public String getColor() {

    return color;
  }

  public String getCardValue() {

    return cardValue;
  }
}
