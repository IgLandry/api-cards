package org.testjava.apijeudecartes.models;

import org.testjava.apijeudecartes.models.types.CardValues;
import org.testjava.apijeudecartes.models.types.Cards;
import org.testjava.apijeudecartes.models.types.Colors;

public class CardImpl implements Cards {

  private final Colors color;
  private final CardValues cardValue;

  public CardImpl(Colors color, CardValues cardValue) {

    this.color = color;
    this.cardValue = cardValue;
  }

  @Override
  public Colors getColor() {

    return this.color;
  }

  @Override
  public CardValues getCardValue() {

    return this.cardValue;
  }

  @Override
  public int compareTo(Cards card) {

    final var compareTo = this.cardValue.compareTo(card.getCardValue());
    if (compareTo != 0) {
      return compareTo;
    } else {
      return this.color.compareTo(card.getColor());
    }
  }
}
