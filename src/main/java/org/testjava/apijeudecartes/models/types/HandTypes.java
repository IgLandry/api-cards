package org.testjava.apijeudecartes.models.types;

public enum HandTypes {
  TRIE,
  NON_TRIE
}
