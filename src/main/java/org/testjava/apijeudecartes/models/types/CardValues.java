package org.testjava.apijeudecartes.models.types;

public enum CardValues {
  AS("A"),
  DEUX("2"),
  TROIS("3"),
  QUATRE("4"),
  CINQ("5"),
  SIX("6"),
  SEPT("7"),
  HUIT("8"),
  NEUF("9"),
  DIX("10"),
  VALET("J"),
  DAME("Q"),
  ROI("K");

  private final String value;

  CardValues(String v) {

    this.value = v;
  }

  public String getValue() {

    return this.value;
  }
}
