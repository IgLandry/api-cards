package org.testjava.apijeudecartes.models.types;

public enum Colors {
  CARREAUX("carreaux"),
  COEUR("coeur"),
  PIQUE("pique"),
  TREFLE("trefle");
  private final String colorValue;

  Colors(String color) {

    this.colorValue = color;
  }

  public String getColorValue() {

    return this.colorValue;
  }
}
