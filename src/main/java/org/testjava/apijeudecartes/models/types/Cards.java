package org.testjava.apijeudecartes.models.types;

public interface Cards extends Comparable<Cards> {

  Colors getColor();

  CardValues getCardValue();
}
